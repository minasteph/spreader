# README #

Spreader (n): a device for lifting containers by their corner posts.


Spreader is yet another docker webui, with the goal to get all the functionalities of portainer but adding some missing items (like complete docker swarm management).

### What is this repository for? ###

* Docker webUI for single docker hosts or for full docker swarms.
* v0.0.1


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Mina: <mina.claden@gmail.com>
